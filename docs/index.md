Welcome to OpenProducer
=======================

OpenProducer is a framework for building websites for musicians, radio stations, and others in the music industry.

OpenProducer is built as a Drupal distribution, making it both easy to self-host, integrate with other tools and services, and extend with plugins, themes or custom code.

Check out [http://openproducer.org/](http://openproducer.org/) for more information.

This documentation is designed primarily for the following audiences:

* [Configurators](configuration.md): those looking to use OpenProducer to produce a website.
* [System Admins](hosting.md): those looking to host OpenProducer on their own infrastructure.
* [Developers](development.md): those looking to fix bugs, add features or otherwise extend OpenProducer.
