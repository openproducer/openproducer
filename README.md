Welcome to OpenProducer
=======================

OpenProducer is a framework for building websites for musicians, radio stations, and others in the music industry.

OpenProducer is built as a Drupal distribution, making it both easy to self-host, integrate with other tools and services, and extend with plugins, themes or custom code.

Check out [http://openproducer.org/](http://openproducer.org/) for more information.

Technical documentation can be found at [http://docs.openproducer.org](http://docs.openproducer.org).
